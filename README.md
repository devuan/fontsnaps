# fontsnaps

Adjust XFCE4 desktop and window title font size using desktop icons.

Copyright (C) 2017 Joerg Reisenweber

Licence: GPL-2

Depends: xfconf
